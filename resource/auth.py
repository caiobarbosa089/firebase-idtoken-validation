from flask_restx import Resource, reqparse
from controller.auth import AuthController
from settings import api

# Swagger namespace
verify_namespace = api.namespace('verify', description='Verify operations')

'''
Route for /verify/<string:idToken>
'''
@api.doc(params={'idToken': 'idToken'}) # Params in swagger
@verify_namespace.route('/<string:idToken>') # Swagger namespace
class Auth(Resource):
    # Verify idToken
    def get(self, idToken):
        # Call controller
        return AuthController.verify(idToken)

    # Add token in BLACKLIST
    def delete(self, idToken):
        # Call controller
        return AuthController.delete(idToken)
