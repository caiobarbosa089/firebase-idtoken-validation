from firebase_admin import auth
from blacklist import BLACKLIST 

class AuthController():
    '''
    For route /verify
    '''
    # Verify user
    def verify(idToken):
        # Check if idToken is in BLACKLIST
        if idToken in BLACKLIST:
            return { 'valid': False }, 401

        try:
            # Get decode token
            decoded_token = auth.verify_id_token(idToken)
        except:
            # If token is not valid return UNAUTHORIZED
            return { 'valid': False }, 401

        # If everything goes well
        return { 'valid': True, 'user': decoded_token}, 200

    # Delete user
    def delete(idToken):
        # Check if idToken is in BLACKLIST
        if idToken in BLACKLIST:
            return { "message": "It's already logged out" }, 400

        try:
            # Get decode token
            decoded_token = auth.verify_id_token(idToken)
            if decoded_token:
                # Add token in BLACKLIST
                BLACKLIST.add(idToken)
        except:
            # If token is not valid return UNAUTHORIZED
            return { 'message': 'Not a valid token' }, 400

        # If everything goes well
        return { "message": "Logout" }, 200