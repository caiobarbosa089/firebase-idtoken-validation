from firebase_admin import credentials
from dotenv import load_dotenv
from flask_cors import CORS
from flask_restx import Api
from flask import Flask
import firebase_admin
import os

# Launches the app
app = Flask(__name__)

# Load environment variables
load_dotenv()

# ---------------------------
# -- Environment variables --
# ---------------------------
GOOGLE_APPLICATION_CREDENTIALS = os.getenv('GOOGLE_APPLICATION_CREDENTIALS')

# Allow CORS
CORS(app)

# Config firebase
cred = credentials.Certificate(GOOGLE_APPLICATION_CREDENTIALS)
firebase_app = firebase_admin.initialize_app(cred)

# Lauches the api with the app and config Swagger
api = Api(
    app,
    version='1.0',
    title='Firebase verification',
    description='Checks firebase authentication based on ID Token'
)