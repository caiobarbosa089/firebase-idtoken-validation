# firebase-idtoken-validation
This project aims to operate as a microservice for
assist the Back-end to validate an ID token provided by Firebase and sent by Fron-end

## Requirements
Be sure you have **python**, **pip** and **virtualenv** installed.

You can download easily **python** from the [official website](https://www.python.org/downloads/).

Now install the **pip** following the steps on the [official website](https://pip.pypa.io/en/stable/installing/).

Now install the **virtualenv** following the steps on the [official website](https://virtualenv.pypa.io/en/latest/installation.html).

After the installation you can open the terminal and test both python and pip.

- Run `python -v` to get python's respective version if the installation was fully OK.

- Run`pip -v` to get pip's respective version, the same way you test python.

If you have both messages you are ready to go!

The **pip** is used to install and manage project dependencies and run it by command line.

## How to use

Run `virtualenv ambvir --python=python3.8` to create virtual environment.
> **Note:** You can use any other python version.

Run `source ambvir/bin/activate` to start virtual environment.
> **Note:** If you are using Windows run `ambvir\Scripts\Activate`.

Run `pip install -r requirements.txt` to install dependencies.

Run `python app.py` to start de project.

To deactive de virtual enviroment run`deactivate`.

## Structural organization 
📦firebase-idtoken-validation  
 ┣ 📂controller  
 ┃ ┗ 📜auth.py  
 ┣ 📂resource  
 ┃ ┗ 📜auth.py  
 ┣ 📜.env  
 ┣ 📜.gitignore  
 ┣ 📜README.md  
 ┣ 📜app.py  
 ┣ 📜blacklist.py  
 ┣ 📜extensions.py  
 ┣ 📜key.json  
 ┣ 📜requirements.txt  
 ┗ 📜settings.py  