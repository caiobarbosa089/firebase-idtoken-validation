# Lauches App and Api
from settings import *
# Resources
from resource.auth import Auth

# -------------------
# ---- Resources ----
# -------------------
api.add_resource(Auth)

def create_app():
    return app

# Default Flask's init 
if __name__ == '__main__':
    # Lauches the api with the app
    app.run(debug=True, port=5000)
